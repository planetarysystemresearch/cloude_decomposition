PROGRAM cloudemain

  USE cloudedecomp
  
  INTEGER, PARAMETER :: cn = 512
  CHARACTER(len=17) :: dfn = "cloude_decomp.out"
  
  INTEGER :: i, j, fu, ie, cc
  REAL(kind=dp), DIMENSION(4) :: eval
  REAL(kind=dp), DIMENSION(4,4) :: Mm
  REAL(kind=dp), DIMENSION(4,4,4) :: Mms
  CHARACTER(len=cn) :: fn
  
  cc = COMMAND_ARGUMENT_COUNT()
  IF(cc < 1) THEN
    WRITE(*,*) "Error. Give name of the file containing the Mueller matrix as first argument."
    WRITE(*,*) "Optionally, you can give ooutput file name as second argument."
    WRITE(*,*) "If not given, default name of '", TRIM(dfn), "' is used."
    STOP
  END IF

  CALL GET_COMMAND_ARGUMENT(1, fn)
  OPEN(NEWUNIT=fu, FILE=TRIM(fn), STATUS='old', ACTION='read')
  DO i=1,4
    READ(fu, *, IOSTAT=ie) Mm(i,:)
    IF(ie /= 0) THEN
      WRITE(*,'(A,A,A,I0,A)') "Error. Problem reading Muller matrix from file '", &
        TRIM(fn), "', line ", i, "."
      STOP
    END IF
  END DO
  CLOSE(fu)
  
  WRITE(*,*) ""
  WRITE(*,*) "Processing this Mueller matrix:"
  CALL printmat(Mm)
  
  CALL Cloude_decomp(Mm, eval, Mms)
  
  IF(cc >= 2) THEN
    CALL GET_COMMAND_ARGUMENT(2, fn)
  ELSE
    READ(dfn,*) fn
  END IF
  WRITE(*,'(A,A,A)') "'", TRIM(fn), "'"
  OPEN(NEWUNIT=fu, FILE=TRIM(fn), STATUS='replace', ACTION='write')
  esum = SUM(eval)
  DO i=1,4
    WRITE(fu,*) eval(i)
    DO j=1,4
      WRITE(fu,*) Mms(i,j,:)
    END DO
  END DO
  CLOSE(fu)

  WRITE(*,*) ""
  WRITE(*,*) "Cloude decomposition was written in file '", TRIM(fn), "'."

END PROGRAM cloudemain
