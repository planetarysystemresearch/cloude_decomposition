# README #

This is a Fortran 2008 implementation of Cloude decomposition of Mueller matrix. This is based on Savenkov et al. (2021, see folder 'info') and Savenkov et al. (2009).

There is a Fortran module that implements the method, and a main program that shows one possible use case. You can compile with Makefile, you need LAPACK libraries installed.

References:

S.N. Savenkov, A.A. Kokhanovsky, Y.A. Oberemok, I.S. Kolomiets, A.S. Klimov (2021). Polarimetry of soil and vegetation in the visible II: Mueller matrix decompositions. Journal of Quantitative Spectroscopy & Radiative Transfer 268, 107622.

S.N. Savenkov (2009). Jones and Mueller matrices: structure, symmetry relations and information content. In: Kokhanovsky (ed), Light Scattering Reviews 4, 2009.
