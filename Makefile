###############################################
# Makefile for Cloude decomposition
# Antti Penttilä

# Compiler
FC = gfortran

FOPT += -fcheck=bounds,pointer -Wall -O1 -ffree-form -std=f2008

# If you need to tell the compiler where to find BLAS and LAPACK, tell it here
LIBS += -L/usr/local/lib

all : cloude_decomp

.PHONY : clean

cloude_decomp.mod : cloude_decomp.f
	$(FC) $(FOPT) -o cloude_decomp.o -c cloude_decomp.f

cloude_decomp_main.o : cloude_decomp_main.f
	$(FC) $(FOPT) -c cloude_decomp_main.f

cloude_decomp : cloude_decomp.mod cloude_decomp_main.o
	$(FC) $(FOPT) -o cloude_decomp cloude_decomp.o cloude_decomp_main.o $(LIBS) -llapack -lblas

clean : 
	rm -rf *.o
	rm -rf *.mod
	rm -rf *.exe
	rm -rf cloude_decomp
