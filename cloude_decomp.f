MODULE cloudedecomp

  USE, INTRINSIC :: iso_fortran_env
  
  PRIVATE
  
  INTEGER, PARAMETER :: dp = REAL64
  COMPLEX(kind=dp), PARAMETER :: co = (0.0_dp, 1.0_dp)
  
  INTERFACE printvec
    MODULE PROCEDURE printvecr, printvecc
  END INTERFACE printvec
  
  INTERFACE printmat
    MODULE PROCEDURE printmatr, printmatc
  END INTERFACE printmat

  PUBLIC dp, printvec, printmat, Mueller_to_coherency, &
    Jones_to_Mueller, Cloude_decomp

CONTAINS
  

! Mueller matrix to 'Coherency matrix'
SUBROUTINE Mueller_to_coherency(Mm, Cm)

  REAL(kind=dp), DIMENSION(4,4), INTENT(IN) :: Mm
  COMPLEX(kind=dp), DIMENSION(4,4), INTENT(OUT) :: Cm
  
  COMPLEX(kind=dp), PARAMETER :: co = (0.0_dp, 1.0_dp)
  
  Cm(1,1) = Mm(1,1)+Mm(2,2)+Mm(3,3)+Mm(4,4)
  Cm(1,2) = Mm(1,2)+Mm(2,1)-co*Mm(3,4)+co*Mm(4,3)
  Cm(1,3) = Mm(1,3)+co*Mm(2,4)+Mm(3,1)-co*Mm(4,2)
  Cm(1,4) = Mm(1,4)-co*Mm(2,3)+co*Mm(3,2)+Mm(4,1)
  Cm(2,1) = Mm(1,2)+Mm(2,1)+co*Mm(3,4)-co*Mm(4,3)
  Cm(2,2) = Mm(1,1)+Mm(2,2)-Mm(3,3)-Mm(4,4)
  Cm(2,3) = co*Mm(1,4)+Mm(2,3)+Mm(3,2)-co*Mm(4,1)
  Cm(2,4) = -co*Mm(1,3)+Mm(2,4)+co*Mm(3,1)+Mm(4,2)
  Cm(3,1) = Mm(1,3)-co*Mm(2,4)+Mm(3,1)+co*Mm(4,2)
  Cm(3,2) = -co*Mm(1,4)+Mm(2,3)+Mm(3,2)+co*Mm(4,1)
  Cm(3,3) = Mm(1,1)-Mm(2,2)+Mm(3,3)-Mm(4,4)
  Cm(3,4) = co*Mm(1,2)-co*Mm(2,1)+Mm(3,4)+Mm(4,3)
  Cm(4,1) = Mm(1,4)+co*Mm(2,3)-co*Mm(3,2)+Mm(4,1)
  Cm(4,2) = co*Mm(1,3)+Mm(2,4)-co*Mm(3,1)+Mm(4,2)
  Cm(4,3) = -co*Mm(1,2)+co*Mm(2,1)+Mm(3,4)+Mm(4,3)
  Cm(4,4) = Mm(1,1)-Mm(2,2)-Mm(3,3)+Mm(4,4)
  
  Cm = 0.25_dp * Cm

END SUBROUTINE Mueller_to_coherency
  

! Jones matrix to Mueller matrix
SUBROUTINE Jones_to_Mueller(Jm, Mm)

  COMPLEX(kind=dp), DIMENSION(2,2), INTENT(IN) :: Jm
  REAL(kind=dp), DIMENSION(4,4), INTENT(OUT) :: Mm
  
  Mm(1,1) = REAL((CONJG(Jm(1,1))*Jm(1,1)+CONJG(Jm(2,1))*Jm(2,1)) + &
    (CONJG(Jm(1,2))*Jm(1,2)+CONJG(Jm(2,2))*Jm(2,2)),kind=dp)
  Mm(1,2) = REAL((CONJG(Jm(1,1))*Jm(1,1)+CONJG(Jm(2,1))*Jm(2,1)) + &
    (-CONJG(Jm(1,2))*Jm(1,2)-CONJG(Jm(2,2))*Jm(2,2)),kind=dp)
  Mm(1,3) = REAL((CONJG(Jm(1,2))*Jm(1,1)+CONJG(Jm(2,2))*Jm(2,1)) + &
    (CONJG(Jm(1,1))*Jm(1,2)+CONJG(Jm(2,1))*Jm(2,2)),kind=dp)
  Mm(1,4) = REAL(-co*(CONJG(Jm(1,2))*Jm(1,1)+CONJG(Jm(2,2))*Jm(2,1)) + &
    co*(CONJG(Jm(1,1))*Jm(1,2)+CONJG(Jm(2,1))*Jm(2,2)),kind=dp)
  Mm(2,1) = REAL((CONJG(Jm(1,1))*Jm(1,1)-CONJG(Jm(2,1))*Jm(2,1)) + &
    (CONJG(Jm(1,2))*Jm(1,2)-CONJG(Jm(2,2))*Jm(2,2)),kind=dp)
  Mm(2,2) = REAL((CONJG(Jm(1,1))*Jm(1,1)-CONJG(Jm(2,1))*Jm(2,1)) + &
    (-CONJG(Jm(1,2))*Jm(1,2)+CONJG(Jm(2,2))*Jm(2,2)),kind=dp)
  Mm(2,3) = REAL((CONJG(Jm(1,2))*Jm(1,1)-CONJG(Jm(2,2))*Jm(2,1)) + &
    (CONJG(Jm(1,1))*Jm(1,2)-CONJG(Jm(2,1))*Jm(2,2)),kind=dp)
  Mm(2,4) = REAL(-co*(CONJG(Jm(1,2))*Jm(1,1)-CONJG(Jm(2,2))*Jm(2,1)) + &
    co*(CONJG(Jm(1,1))*Jm(1,2)-CONJG(Jm(2,1))*Jm(2,2)),kind=dp)
  Mm(3,1) = REAL((CONJG(Jm(2,1))*Jm(1,1)+CONJG(Jm(1,1))*Jm(2,1)) + &
    (CONJG(Jm(2,2))*Jm(1,2)+CONJG(Jm(1,2))*Jm(2,2)),kind=dp)
  Mm(3,2) = REAL((CONJG(Jm(2,1))*Jm(1,1)+CONJG(Jm(1,1))*Jm(2,1)) + &
    (-CONJG(Jm(2,2))*Jm(1,2)-CONJG(Jm(1,2))*Jm(2,2)),kind=dp)
  Mm(3,3) = REAL((CONJG(Jm(2,2))*Jm(1,1)+CONJG(Jm(1,2))*Jm(2,1)) + &
    (CONJG(Jm(2,1))*Jm(1,2)+CONJG(Jm(1,1))*Jm(2,2)),kind=dp)
  Mm(3,4) = REAL(co*(CONJG(Jm(2,2))*Jm(1,1)+CONJG(Jm(1,2))*Jm(2,1)) + &
    co*(CONJG(Jm(2,1))*Jm(1,2)+CONJG(Jm(1,1))*Jm(2,2)),kind=dp)
  Mm(4,1) = REAL((co*CONJG(Jm(2,1))*Jm(1,1)-co*CONJG(Jm(1,1))*Jm(2,1)) + &
    (co*CONJG(Jm(2,2))*Jm(1,2)-co*CONJG(Jm(1,2))*Jm(2,2)),kind=dp)
  Mm(4,2) = REAL((co*CONJG(Jm(2,1))*Jm(1,1)-co*CONJG(Jm(1,1))*Jm(2,1)) + &
    (-co*CONJG(Jm(2,2))*Jm(1,2)+co*CONJG(Jm(1,2))*Jm(2,2)),kind=dp)
  Mm(4,3) = REAL((co*CONJG(Jm(2,2))*Jm(1,1)-co*CONJG(Jm(1,2))*Jm(2,1)) + &
    (co*CONJG(Jm(2,1))*Jm(1,2)-co*CONJG(Jm(1,1))*Jm(2,2)),kind=dp)
  Mm(4,4) = REAL(-co*(co*CONJG(Jm(2,2))*Jm(1,1)-co*CONJG(Jm(1,2))*Jm(2,1)) + &
    co*(co*CONJG(Jm(2,1))*Jm(1,2)-co*CONJG(Jm(1,1))*Jm(2,2)),kind=dp)
  
  Mm = 0.5_dp * Mm

END SUBROUTINE Jones_to_Mueller


! Cloude decomposition of Mueller matrix 'Mm'.
! Returns probabilities ('eval') and Mueller matrices ('Mms')
SUBROUTINE Cloude_decomp(Mm, eval, Mms)

  REAL(kind=dp), DIMENSION(4,4), INTENT(IN) :: Mm
  REAL(kind=dp), DIMENSION(4), INTENT(OUT) :: eval
  REAL(kind=dp), DIMENSION(4,4,4), INTENT(OUT) :: Mms
  
  INTEGER, PARAMETER :: wd = 1024
  
  INTEGER :: ok, i
  REAL(kind=dp), DIMENSION(8) :: workr
  COMPLEX(kind=dp), DIMENSION(wd) :: workz
  COMPLEX(kind=dp), DIMENSION(4,2,2) :: Jms
  COMPLEX(kind=dp), DIMENSION(4,4) :: Cm
  
  CALL Mueller_to_coherency(Mm, Cm)
  
  ! WRITE(*,*) ""
  ! WRITE(*,*) "coherency"
  ! CALL printmat(Cm)
  
  ! Eigenvalues, LAPACK, Hermitian matrix
  CALL ZHEEV('V', 'U', 4, Cm, 4, eval, workz, wd, workr, ok)
  ! WRITE(*,*) ""
  ! WRITE(*,*) ok
  
  ! Sort eigenvalues and matrices
  CALL eigensort(eval, Cm)

  ! WRITE(*,*) ""
  ! WRITE(*,*) "eval"
  ! CALL printvec(eval)
  ! WRITE(*,*) ""
  ! WRITE(*,*) "Evec"
  ! CALL printmat(TRANSPOSE(Cm))
  
  ! 'Jones' matrices
  DO i=1,4
    Jms(i,1,1) = Cm(1,i) + Cm(2,i)
    Jms(i,1,2) = Cm(3,i) -co*Cm(4,i)
    Jms(i,2,1) = Cm(3,i) +co*Cm(4,i)
    Jms(i,2,2) = Cm(1,i) - Cm(2,i)
  END DO

  ! Normalize eigenvalues to probabilities
  eval(:) /= SUM(eval)
  
  ! Mueller matrices
  DO i=1,4
    CALL Jones_to_Mueller(Jms(i,:,:), Mms(i,:,:))
    ! WRITE(*,*) ""
    ! CALL printmat(Mms(i,:,:))
  END DO


END SUBROUTINE Cloude_decomp


! Sorts eigenvalues and eigenvectors from largest to smallest
! Using bubblesort (Wikipedia, own implementation)
SUBROUTINE eigensort(eval, Evec)

  REAL(kind=dp), DIMENSION(4), INTENT(INOUT) :: eval
  COMPLEX(kind=dp), DIMENSION(4,4), INTENT(INOUT) :: Evec
  
  INTEGER :: i, n, newn
  REAL(kind=dp) :: xr
  COMPLEX(kind=dp), DIMENSION(4) :: xc
  
  n = 4
  DO WHILE(n>1)
    newn = 0
    DO i=2,n
      IF(eval(i-1) < eval(i)) THEN
        xr = eval(i-1)
        eval(i-1) = eval(i)
        eval(i) = xr
        xc = Evec(:,i-1)
        Evec(:,i-1) = Evec(:,i)
        Evec(:,i) = xc
        newn = i
      END IF
    END DO
    n = newn    
  END DO

END SUBROUTINE eigensort


SUBROUTINE printvecr(v)

  REAL(kind=dp), DIMENSION(4), INTENT(IN) :: v
  
  WRITE(*,'(A,F14.10)') "(1) ", v(1)
  WRITE(*,'(A,F14.10)') " (2) ", v(2)
  WRITE(*,'(A,F14.10)') "  (3) ", v(3)
  WRITE(*,'(A,F14.10)') "   (4) ", v(4)

END SUBROUTINE printvecr


SUBROUTINE printvecc(v)

  COMPLEX(kind=dp), DIMENSION(4), INTENT(IN) :: v
  
  WRITE(*,'(A,2F14.10)') "(1) ", v(1)
  WRITE(*,'(A,2F14.10)') " (2) ", v(2)
  WRITE(*,'(A,2F14.10)') "  (3) ", v(3)
  WRITE(*,'(A,2F14.10)') "   (4) ", v(4)

END SUBROUTINE printvecc


SUBROUTINE printmatr(m)

  REAL(kind=dp), DIMENSION(4,4), INTENT(IN) :: m
  
  INTEGER :: li
  
  DO li=1,4
    WRITE(*,'(A,I0,A,F14.10)') "(", li, ",1) ", m(li,1)
    WRITE(*,'(A,I0,A,F14.10)') " (", li, ",2) ", m(li,2)
    WRITE(*,'(A,I0,A,F14.10)') "  (", li, ",3) ", m(li,3)
    WRITE(*,'(A,I0,A,F14.10)') "   (", li, ",4) ", m(li,4)
  END DO

END SUBROUTINE printmatr


SUBROUTINE printmatc(m)

  COMPLEX(kind=dp), DIMENSION(4,4), INTENT(IN) :: m
  
  INTEGER :: li
  
  DO li=1,4
    WRITE(*,'(A,I0,A,2F14.10)') "(", li, ",1) ", m(li,1)
    WRITE(*,'(A,I0,A,2F14.10)') " (", li, ",2) ", m(li,2)
    WRITE(*,'(A,I0,A,2F14.10)') "  (", li, ",3) ", m(li,3)
    WRITE(*,'(A,I0,A,2F14.10)') "   (", li, ",4) ", m(li,4)
  END DO

END SUBROUTINE printmatc


END MODULE cloudedecomp
